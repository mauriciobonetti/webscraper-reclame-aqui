const puppeteer = require('puppeteer');
const fs = require('fs')
const { AsyncParser } = require('json2csv')

const BASE_URL = 'http://www.reclameaqui.com.br/empresa'

async function lista_solicitacoes(empresa, status){
    const url = `${BASE_URL}/${empresa}/lista-reclamacoes`
    try{
        const browser = await puppeteer.launch({headless: false})
        const page = await browser.newPage();
        await page.goto(url, { waitUntil: 'networkidle2'});
        const tr = await page.$$eval('a#link-list-complain-all span.company-index-label-value', as => as.map(a => a.innerHTML));
        const totalReclamacoes = +tr[0]
        const numPaginas = Math.ceil((totalReclamacoes / 100))
        let paginaAtual = 1;

        const data = []

        while (paginaAtual <= numPaginas) {
            const url = `${BASE_URL}/${empresa}/lista-reclamacoes/?pagina=${paginaAtual}`

            await page.goto(url, { waitUntil: 'networkidle2'});
            const hrefs = await page.$$eval('a.link-complain-id-complains', as => as.map(a => a.href));

            const promises = hrefs.map(async link => {
                try{
                    await page.goto(link, {waitUntil: 'networkidle2'})

                    const header = await page.$$eval('ul.local-date', as => as[0].childNodes[7].innerHTML)
                    const date = header.replace(`<i class="fa fa-calendar"></i>`, ``)
                    
                    const status = await page.$$eval('div.upshot-seal  img', as => as[0].alt)
                    const dataAvaliacao = await page.$$eval('div.header-date p.date', p => p[0] ? p[0].innerHTML : "Não avaliado")
                    const nota = await page.$$eval('div.score-seal div.img-circle p.ng-binding', p => p[0] ? p[0].innerHTML: "Sem Nota")
                    const respostasEmpresa = await page.$$eval('div.business-reply', div => div.length)

                    return {
                        date,
                        status,
                        dataAvaliacao,
                        nota,
                        respostasEmpresa
                    }
                    
                } catch(error) {
                    console.log(error)
                    return null
                }
            })
            const itens = await Promise.all(promises)
            console.log(`Adicionados ${itens.length} itens, total: ${data.length + itens.length}`)
            data.push(...itens)
            paginaAtual += 1
        }

        const json = JSON.stringify(data)
        await fs.writeFile('dados.json', json, 'utf8', async () => {
            await browser.close()
        });
    } catch(error){
        console.log(error)
    }

}

const reader  = fs.readFileSync('empresas.txt')
const empresas = reader.toString().split('\n')

for (let i = 0; i < empresas.length; i += 1) {
    const empresa = empresas[i]
    lista_solicitacoes(empresa, 'EVALUATED')
}